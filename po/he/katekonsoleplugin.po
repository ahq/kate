# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Diego Iastrubni <elcuco@kde.org>, 2014.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
# SPDX-FileCopyrightText: 2024 Yaron Shahrabani <sh.yaron@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: katekonsoleplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-25 00:40+0000\n"
"PO-Revision-Date: 2024-05-22 15:25+0300\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: צוות התרגום של KDE ישראל\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && "
"n % 10 == 0) ? 2 : 3));\n"
"X-Generator: Lokalize 24.02.2\n"

#: kateconsole.cpp:70
#, kde-format
msgid "You do not have enough karma to access a shell or terminal emulation"
msgstr "אין לך מספיק קארמה כדי לגשת למעטפת או להדמיית המסוף"

#: kateconsole.cpp:118 kateconsole.cpp:148 kateconsole.cpp:769
#, kde-format
msgid "Terminal"
msgstr "מסוף"

#: kateconsole.cpp:157
#, kde-format
msgctxt "@action"
msgid "&Pipe to Terminal"
msgstr "&ניתוב למסוף"

#: kateconsole.cpp:161
#, kde-format
msgctxt "@action"
msgid "S&ynchronize Terminal with Current Document"
msgstr "&סנכרון המסוף עם המסמך הנוכחי"

#: kateconsole.cpp:165
#, kde-format
msgctxt "@action"
msgid "Run Current Document"
msgstr "הרצת המסמך הנוכחי"

#: kateconsole.cpp:170 kateconsole.cpp:556
#, kde-format
msgctxt "@action"
msgid "S&how Terminal Panel"
msgstr "ה&צגת לוח מסוף"

#: kateconsole.cpp:176
#, kde-format
msgctxt "@action"
msgid "&Focus Terminal Panel"
msgstr "התמ&קדות בלוח המסוף"

#: kateconsole.cpp:182
#, kde-format
msgctxt "@action"
msgid "&Split Terminal Vertically"
msgstr "&פיצול המסוף אנכית"

#: kateconsole.cpp:187
#, kde-format
msgctxt "@action"
msgid "&Split Terminal Horizontally"
msgstr "פיצול המסוף &אופקית"

#: kateconsole.cpp:192
#, kde-format
msgctxt "@action"
msgid "&New Terminal Tab"
msgstr "לשונית מסוף &חדשה"

#: kateconsole.cpp:355
#, kde-format
msgid ""
"Konsole not installed. Please install konsole to be able to use the terminal."
msgstr "Konsole לא מותקן. נא להתקין את konsole כדי לאפשר שימוש במסוף."

#: kateconsole.cpp:431
#, kde-format
msgid ""
"Do you really want to pipe the text to the console? This will execute any "
"contained commands with your user rights."
msgstr ""
"לנתב את הטקסט למסוף? עשוי להפעיל פקודות כלולות כלשהןעם הרשאות המשתמש שלך."

#: kateconsole.cpp:432
#, kde-format
msgid "Pipe to Terminal?"
msgstr "האם לנתב למסוף?"

#: kateconsole.cpp:433
#, kde-format
msgid "Pipe to Terminal"
msgstr "ניתוב למסוף"

#: kateconsole.cpp:497
#, kde-format
msgid "Not a local file: '%1'"
msgstr "לא קובץ מקומי: ‚%1’"

#: kateconsole.cpp:530
#, kde-format
msgid ""
"Do you really want to Run the document ?\n"
"This will execute the following command,\n"
"with your user rights, in the terminal:\n"
"'%1'"
msgstr ""
"להריץ את המסמך?\n"
"הפעולה הזאת תפעיל את הפקודה הבאה,\n"
"עם הרשאות המשתמש שלך, במסוף:\n"
"‚%1’"

#: kateconsole.cpp:537
#, kde-format
msgid "Run in Terminal?"
msgstr "להריץ במסוף?"

#: kateconsole.cpp:538
#, kde-format
msgid "Run"
msgstr "הרצה"

#: kateconsole.cpp:553
#, kde-format
msgctxt "@action"
msgid "&Hide Terminal Panel"
msgstr "ה&סתרת לוח מסוף"

#: kateconsole.cpp:564
#, kde-format
msgid "Defocus Terminal Panel"
msgstr "ביטול מיקוד ללוח מסוף"

#: kateconsole.cpp:565 kateconsole.cpp:566
#, kde-format
msgid "Focus Terminal Panel"
msgstr "התמקדות על לוח מסוף"

#: kateconsole.cpp:677
#, kde-format
msgid "Terminal Synchronization Mode"
msgstr "מצב סנכרון מסוף"

#: kateconsole.cpp:680
#, kde-format
msgid "&Don't synchronize terminal tab with current document"
msgstr "לא &לסנכרן את לשונית המסוף עם המסמך הנוכחי"

#: kateconsole.cpp:683
#, kde-format
msgid ""
"&Automatically synchronize the current terminal tab with the current "
"document when possible"
msgstr "ל&סנכרן את לשונית המסוף הנוכחית מול המסמך הנוכחי אוטומטית כשאפשר"

#: kateconsole.cpp:686
#, kde-format
msgid ""
"&Automatically create a terminal tab for the directory of the current "
"document and switch to it"
msgstr "לי&צור לשונית מסוף לתיקיית המסמך הנוכחי ולעבור אליה"

#: kateconsole.cpp:693 kateconsole.cpp:714
#, kde-format
msgid "Run in terminal"
msgstr "הרצה במסוף"

#: kateconsole.cpp:695
#, kde-format
msgid "&Remove extension"
msgstr "ה&סרת סיומת"

#: kateconsole.cpp:700
#, kde-format
msgid "Prefix:"
msgstr "קידומת:"

#: kateconsole.cpp:708
#, kde-format
msgid "&Show warning next time"
msgstr "להציג א&זהרה בפעם הבאה"

#: kateconsole.cpp:710
#, kde-format
msgid ""
"The next time '%1' is executed, make sure a warning window will pop up, "
"displaying the command to be sent to terminal, for review."
msgstr ""
"עם ההפעלה הבאה של ‚%1’, נא לוודא שיקפוץ חלון אזהרה, שיציג את הפקודה שתישלח "
"למסוף, לסקירה."

#: kateconsole.cpp:721
#, kde-format
msgid "Set &EDITOR environment variable to 'kate -b'"
msgstr "לה&גדיר את משתנה הסביבה EDITOR ל־‚kate -b’"

#: kateconsole.cpp:724
#, kde-format
msgid ""
"Important: The document has to be closed to make the console application "
"continue"
msgstr "חשוב: יש לסגור את המסמך כדי לאפשר ליישום המסוף להמשיך"

#: kateconsole.cpp:727
#, kde-format
msgid "Hide Konsole on pressing 'Esc'"
msgstr "להסתיר את Konsole בלחיצה על ‚Esc’"

#: kateconsole.cpp:730
#, kde-format
msgid ""
"This may cause issues with terminal apps that use Esc key, for e.g., vim. "
"Add these apps in the input below (Comma separated list)"
msgstr ""
"יכול לגרום לבעיות עם יישומי מסוף שמשתמשים במקש Esc, למשל: vim. יש להוסיף את "
"היישומים האלה בקלט שלהלן (רשימה מופרדת בפסיקים)"

#: kateconsole.cpp:774
#, kde-format
msgid "Terminal Settings"
msgstr "הגדרות מסוף"

#. i18n: ectx: Menu (tools)
#: ui.rc:6
#, kde-format
msgid "&Tools"
msgstr "&כלים"

#~ msgid "Sorry, cannot cd into '%1'"
#~ msgstr "אי אפשר לעבור לתיקייה ‏‚%1’, מחילה"

#, fuzzy
#~ msgid "Kate Terminal"
#~ msgstr "מסוף"

#, fuzzy
#~| msgid "Terminal"
#~ msgid "Terminal Panel"
#~ msgstr "מסוף"
